import React, { Component } from "react";
import "./App.css";
import Header from "./Componenets/commons/Header/Header";
import SecondaryNavbar from "./Componenets/commons/SecondaryNavbar/SecondaryNavbar";
import Beta from "./Componenets/Beta/Beta";



class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <SecondaryNavbar />
        <Beta />
      </div>
    );
  }
}

export default App;
