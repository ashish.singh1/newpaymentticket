import React, { Component } from "react";
// import DatePicker from '../../commons/DatePicker/DatePicker'
import "./style.css";

class SideFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="col-md-2">
        <div className="side-filter">
          <form>
            <div>
              <div className="form-group">
                <label htmlFor="invoice-number">Invoice Number</label>
                <input
                  type="text"
                  className="form-control"
                  id="invoice-number"
                  placeholder="Invoice Number"
                />
              </div>
              <div className="form-group">
                <label htmlFor="vendor-name">Vendor</label>
                <input
                  type="text"
                  className="form-control"
                  id="vendor-name"
                  placeholder="Vendor Name"
                />
              </div>
              <hr />
              <div className="form-group">
                <label htmlFor="vendor-name">Date</label>
                <input
                  type="date"
                  className="form-control"
                  id="date"
                  placeholder="Date"
                />
              </div>
              <div className="form-group">
                <label htmlFor="vendor-name">Due Date</label>
                <input
                  type="date"
                  className="form-control"
                  id="due-date"
                  placeholder="Due Date"
                />
              </div>
            </div>
            <div className="filter-button">
              <button className="btn btn-default btn-apply-filter">
                <img
                  src="https://img.icons8.com/windows/19/0085DF/sorting-options.png"
                  alt="filter"
                />
                Apply Filter
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default SideFilter;
