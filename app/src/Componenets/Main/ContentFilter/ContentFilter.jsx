import React, { Component } from "react";
import './style.css'

class ContentFilter extends Component {
  render() {
    return (
      <div className="col-md-12">
        <div className="content-filter">
          <form className="form">
            <div className="form-group">
              <label htmlFor="schedule-date">
                Schedule Date
              </label>
              <input
                type="date"
                className="form-control"
                id="schedule-date"
                placeholder="Schedule Date"
              />
            </div>
            <div className="form-group">
              <label htmlFor="transfer-type-select">
                Transfer Type
              </label>
              <select className="form-control" id="transfer-type-select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="transfer-type-select">
                Policy Type
              </label>
              <select className="form-control" id="transfer-type-select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="description-text">
                Description
              </label>
              <input
                type="text"
                className="form-control"
                id="description-text"
                placeholder="Description"
              />
            </div>
            <div className="form-group">
              <label htmlFor="vendorId">
                Vendor ID
              </label>
              <input
                type="text"
                className="form-control"
                id="vendorId"
                placeholder="Description"
              />
            </div>
            <div className="form-group">
              <label htmlFor="vendorName">
                Vendor Name
              </label>
              <input
                type="text"
                className="form-control"
                id="vendorName"
                placeholder="Description"
              />
            </div>
            <div className="form-group">
              <label htmlFor="createdBy">
                Created By
              </label>
              <input
                type="text"
                className="form-control"
                id="createdBy"
                placeholder="Description"
              />
            </div>
            <div className="form-group">
              <label htmlFor="beneficiary-type-select">
                Beneficiary Type
              </label>
              <select className="form-control" id="transfer-type-select">
              <option></option>
                <option>Vendor</option>
                <option>Employees</option>
              </select>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default ContentFilter;
