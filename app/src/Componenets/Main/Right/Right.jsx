import React, { Component } from 'react'
import './style.css'

class Right extends Component {
  render() {
    return (
      <div className='col-md-2'>
        <div className="right-side">
          <div>
            <h3  className="total-invoice">total invoices</h3>
            <h4 className='total-invoice-value'>0</h4>
          </div>
          <div>
          <h3  className="total-amount">total amount</h3>
          <h4 className='total-amount-value'>0</h4>
          </div>
        </div>
      </div>
    )
  }
}

export default Right;