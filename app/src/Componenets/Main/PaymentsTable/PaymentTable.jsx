import React, { Component } from "react";
import "./style.css";

class PaymentTable extends Component {
  render() {
    return (
      <div className="payment-ticket-table">
        <table className="ticket-table">
          <tr>
            <th>#</th>
            <th>invoice #</th>
            <th>vendor</th>
            <th>due date</th>
            <th>total payable</th>
            <th>balance due</th>
            <th>payable</th>
            <th>&nbsp;</th>
          </tr>

          <tr>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
          </tr>
        </table>
      </div>
    );
  }
}

export default PaymentTable;
