import React,{ Component } from 'react'
import $ from 'jquery'

export default class DatePicker extends Component {
  componentDidMount () {
    $('.datepicker').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy'
    })
  }
  render () {
    return (
      <>
        <input type='text' className='datepicker form-control'  placeholder="Select Date"/>
      </>
    )
  }
}
