import React, { Component } from "react";
import "./style.css";

class SecondaryNavbar extends Component {
  render() {
    return (
      <div className="secondary-navbar">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 navbar-center">
              <ul className="tabs">
                <li className="tab active">beneficiary payments</li>
                <li className="tab">import via excel</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SecondaryNavbar;
