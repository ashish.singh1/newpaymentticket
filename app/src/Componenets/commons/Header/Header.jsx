import React from "react";
import './style.css'

function Header() {
  return (
    <div className="header">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4 header-text">
            <img
              src="https://img.icons8.com/windows/32/000000/refund-2.png"
              alt="icon"
            />
            <h3>New Payment Ticket</h3>
          </div>
          <div className="col-md-8"/>
        </div>
      </div>
    </div>
  );
}

export default Header;
