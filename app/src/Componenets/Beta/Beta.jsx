import React, { Component } from 'react'
import Select from 'react-select'
import './style.scss'

class Beta extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedOption: ''
    }
  }

  handleChange = selectedOption => {
    this.setState = {
      selectedOption
    }
  }

  render () {
    const defaultOption = [
      {
        label: 'Dennis Crist',
        value: 24,
        ifsc: 'SBI12323',
        name: 'HDFC',
        account_number: '13123213',
        default: false
      },
      {
        label: 'Dennis Crist',
        value: 23,
        ifsc: 'SBI 1221323',
        name: 'SBI',
        account_number: '12349842303848',
        default: false
      }
    ]

    const options = [
      { value: 'chocolate', label: 'Chocolate' },
      { value: 'strawberry', label: 'Strawberry' },
      { value: 'vanilla', label: 'Vanilla' }
    ]

    const colourStyles = {
      control: styles => ({
        ...styles,
        backgroundColor: 'white',
        width: '200px'
      }),
      option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
          ...styles,
          textAlign: 'left'
        }
      },
      input: styles => ({ ...styles }),
      placeholder: styles => ({ ...styles }),
      singleValue: (styles, { data }) => ({ ...styles })
    }

    return (
      <div className='beta-testing'>
        <pre>{this.state.selectedOption || null}</pre>
        <div className='card'>
          <div className='filters'>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
                value={this.state.selectedOption}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
            <div className='filter'>
              <Select
                options={options}
                styles={colourStyles}
                onChange={this.handleChange}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Beta
